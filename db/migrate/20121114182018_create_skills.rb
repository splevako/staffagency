class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :name
      t.integer :employee_id, :primary => true
      t.integer :vacancy_id, :primary => true
      t.timestamps
    end     
  end
end
