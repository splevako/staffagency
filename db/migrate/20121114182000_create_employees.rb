class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.string :contact_info
      t.string :status
      t.decimal :salary
      #t.string :skill
      #t.string :employee_id, :primary => true
      t.timestamps
    end    
  end
end
