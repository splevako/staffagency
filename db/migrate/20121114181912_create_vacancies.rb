class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :position
      t.date :creationDate
      t.decimal :expiration
      t.decimal :salary
      t.string :contact_info
      #t.string :skill
      #t.string :vacancy_id, :primary => true
      t.timestamps
    end    
  end
  
  def self.down
    drop_table :vacancies
  end
end
