# encoding: utf-8
require 'test_helper'

class EmployeeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  fixtures :all
  
  test "English name" do
    jon = employees(:jon)
    j1 = Employee.new(jon)
    assert !j1.save
  end
  
  test "Cyrillic name" do
    sergey = employees(:sergey)
    s1 = skills(:eng)
    p1 = Employee.new(sergey,s1)
    assert p1.save
  end
  
  test "Empty employee" do
    epmty1 = Employee.new
    assert empty1.invalid?
    assert empty1.errors[:name].any?
  end
  
end
