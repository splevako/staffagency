# encoding: utf-8
require 'test_helper'

class SkillTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  fixtures :all
  
  test "Valid skill" do
    s1 = skills(:eng)
    skill = Skill.new(s1)
    assert skill.save
  end
  
  test "Empty skill is not valid" do
    skill = Skill.new
    assert !skill.save
  end
  
  test "Odd skill name" do
    skill = Skill.new(:name => "I am odd skil name") 
    assert skill.save
  end
  
  test "Skill must be unique" do
    s1 = Skill.new(:name => "Skill")
    s2 = Skill.new(:name => "Skill")
    assert s1.save
    assert !s2.save
  end  
  
end
