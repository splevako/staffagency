# encoding: utf-8

require 'spec_helper'

describe Vacancy do 
  
  fixtures :vacancies, :skills
  
  before(:each) do
    @vacancy = Vacancy.new(:position => "Rails")
  end
  
  it { should have_many(:skills).through(:employees) }
  it { should validate_format_of(:contact_info).with('ma1f0rmed emai1 address') }
  it { should validate_format_of(:contact_info).with('2012-12-12') }
  
  it "find by date in range" do
    v1=Vacancy.new(:position => "Rails", :creationDate => Date.today)
    v2=Vacancy.new(:position => "Ruby", :creationDate => Date.yesterday)
    v1.should_not be_valid
    
    #res = Vacancy.find_vacancies_by_range(3)
    
    #res.size.should eq(2)
  end
  
  it "test 1" do
    @vacancy.position = ""
    @vacancy.should_not be_valid  
  end
  
end