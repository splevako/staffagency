# encoding: utf-8

require 'spec_helper'

describe Employee do
  
  fixtures :employees, :skills
  
  it { should validate_format_of(:contact_info).with('ma1f0rmed emai1 address') }
  it { should validate_format_of(:contact_info).with('2012-12-12') }
  it { should validate_format_of(:name).with('G F K')}
  it { should have_many(:skills).through(:vacancies) }
  it { should_not allow_value("b lah").for(:contact_info)}

  
  before(:each) do
    @employee = Employee.new
  end
  
  it "test1" do
   a = employees(:sergey)
   a.skills.new(:name => "Testing")
   a.should be_valid
  end
  
  it "Работник должен иметь Имя" do 
    e = employees(:one)
    e.should_not be_valid
  end
       
end
