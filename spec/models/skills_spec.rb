# encoding: utf-8

require 'spec_helper'

describe Skill do 
  
  fixtures :skills
  
  before(:each) do
    @skill = Skill.new
  end
  
  it { should validate_uniqueness_of(:name) }
  #it { should belong_to(:vacancy)}
  #it { should belong_to(:employee)}
  
  it "Умение Должно иметь название" do
    @skill.name = nil
    @skill.should_not be_valid   
  end
  
end