class EmailOrPhoneValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add attribute, (options[:message] || "is not a valid phone number (incl. operator code) or email address") unless
      #value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      #value =~ /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/
      value =~/^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]|[1]?[\-|\s|\.]?\(?[0-9]{3}\)?[\-|\s|\.]?[0-9]{3}[\-|\s|\.]?[0-9]{2}[\-|\s|\.]?[0-9]{2})$/
  end
end