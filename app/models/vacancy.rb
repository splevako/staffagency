class Vacancy < ActiveRecord::Base
  #has_many :employees, :through => :skills
  self.per_page = 10
  #set_primary_key :vacancy_id
  has_many :skills
  has_many :employees, :through => :skills
  
  validates :position,:salary,:contact_info,:expiration, :salary, :presence => true
  validates :contact_info, :email_or_phone => true
  
  def self.search(search)
  if search
    where('position LIKE ?', "%#{search}%")
  else
    scoped
  end
  end
  
  def find_vacancies_by_range(dt)
     Vacancy.find(:creationDate => Date.current - dt.. Date.current)
  end
  
  def find_vacancies_by_position(pos)
     Vacancy.find(:position => pos)
  end
     
end
