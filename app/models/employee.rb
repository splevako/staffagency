# encoding: utf-8
class Employee < ActiveRecord::Base
  self.per_page = 10
  #set_primary_key :employee_id
    
  has_many :skills
  has_many :vacancies, :through => :skills
  
  validates :name, :contact_info, :status, :salary, :presence => true
  
  validates_format_of :name, 
  :with => /[а-яА-ЯёЁ $]/,
  :message => "can only contain cyrillic letters and spaces."
  
  validates_length_of :name, 
  :tokenizer => lambda {|str| str.scan(/[а-яА-ЯёЁ]/)},
  :minimum => 3,  
  :message => "Must have only %{count} cyrillic words"
  
  validates_uniqueness_of :contact_info, :allow_blank => false
  
  validates_format_of :contact_info, 
  :with => /^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]|[1]?[\-|\s|\.]?\(?[0-9]{3}\)?[\-|\s|\.]?[0-9]{3}[\-|\s|\.]?[0-9]{2}[\-|\s|\.]?[0-9]{2})$/,
  :message => "is not a valid phone number (incl. operator code) or email address"
  
  #validates :salary, :numeric? => true
  
  def numeric?(object)
    true if Float(object) rescue false
  end
  
  def findByVacancy(name)
     Employee.find(:all, :conditions => ["name = #{name}", true])
  end
  
  def getAllSkills()
    Skill.find(:all, :conditions => ['name = ?', true])
  end

  def findAllValidVacancies()
    Vacancies.find_all_by_skills(self.skills)   
  end  
end
