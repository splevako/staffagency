class Skill < ActiveRecord::Base
  self.per_page = 10
  attr_accessible :name
  belongs_to :employee
  belongs_to :vacancy

  has_many :employees
  has_many :vacancies
  #belongs_to :employee, :class_name => 'Employee'
  #belongs_to :vacancy, :class_name => 'Vacancy'

  #set_primary_key :skill_id
  #set_primary_key :vacancy_id
  
  validates :name, :uniqueness => true
  validates :name, :presence => true
end
