# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $("#vacancies th a, #vacancies .pagination a").live "click", ->
    $.getScript @href
    false

  $("#vacancies_search input").keyup ->
    $.get $("#vacancies_search").attr("action"), $("#vacancies_search").serialize(), null, "script"
    false